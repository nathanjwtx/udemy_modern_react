import React from 'react'
import ReactDOM from 'react-dom'
import { image } from 'faker'

import { CommentDetail } from './CommentDetail';
import { ApprovalCard } from './ApprovalCard';

const App = () => {
  return (
    <div className="ui container comments">
      <ApprovalCard>
        <CommentDetail
         author={"nathan"}
         picture={image.avatar()}
         time={"2:00pm"}
         comment={"Hello world!"}/>
      </ApprovalCard>
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))
