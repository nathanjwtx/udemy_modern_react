import React from 'react'
import ReactDOM from 'react-dom'

const buttonText = 'Click me!'

const App = () => {
  return (
    <div>
      <label className="label" htmlFor={"name"}>
        Enter name:
      </label>
      <input id={"name"} type={"text"} />
      <button style={{backgroundColor:'blue', color:'white'}}>{buttonText}</button>
    </div>
  )
}

ReactDOM.render(
  <App />, document.getElementById('root')
)